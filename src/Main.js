import PropTypes from 'prop-types';
import './Main.css';

export const Main = (props) => {
  return (
    <>
      {props.children}
    </>
  );
};

Main.propTypes = {
  children: PropTypes.element
};
