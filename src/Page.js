import './Page.css';
import { Header } from './Header';
import { Footer } from './Footer';
import { Main } from './Main';
import { photos } from './data';
import { Photos } from './Photos';

export const Page = () => {
  return (
    <>
      <Header />
      <Main>
        <Photos photos={photos.slice(0, 10)}/>
      </Main>
      <Footer />
    </>
  );
};
