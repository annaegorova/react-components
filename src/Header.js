import PropTypes from 'prop-types';
import { HEADER_TITLE } from './constants';

export const Header = ({ title = HEADER_TITLE}) => {
  return (
    <header>
      <h1 style={{textAlign: 'center'}}>{title}</h1>
    </header>
  );
};

Header.propTypes = {
  title: PropTypes.string
};
