import PropTypes from 'prop-types';

export const Photos = ({ photos }) => {
  return (
    <ul>
      {photos.map(photo => (
        <li key={photo.id}>
          <img src={photo.url} alt={photo.title}/>
        </li>
      ))}
    </ul>
  );
};

Photos.propTypes = {
  photos: PropTypes.arrayOf(PropTypes.object).isRequired
};
