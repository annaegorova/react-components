import PropTypes from 'prop-types';
import { FOOTER_TITLE } from './constants';

export const Footer = ({ title = FOOTER_TITLE}) => {
  return (
    <footer>
      <h3 style={{textAlign: 'center'}}>{title}</h3>
    </footer>
  );
};

Footer.propTypes = {
  title: PropTypes.string
};
